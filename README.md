**Manual / Read Me**
<details><summary>Basic Information</summary>
	
	Program: 		ImputAccur.py / ImputAccur.exe
	Version of 2022 01 22
	Authors: Thormann, Starke, Tozzi, Rosenberger
		 University Medical Center Göttingen, Germany
		 Georg-August University Göttingen, Germany
	Contact: Dr. Albert Rosenberger	arosenb@gwdg.de

</details>

<details><summary>1 Preparation of files</summary>

<details><summary>1a Input-files</summary>

> You must provide marker information along with the estimated genotype probability (dosages) as an input file, which is a plain text file.

> The input file need to contain information of each marker in one row.

> Columns can be separated by spaces or tabs. The input file can also be provided as a zip file.
 

	The input file need to have at least three leading columns: 
	(1) xxxx		[free content]
	(2) SNP			[unique name of the genomic marker]
	(3) position		[physical position]

You can add some more leading columns, e.g. for the chromosome, the minor and major allele, assigned genes, etc. (see 1b option-l Number of leading columns)

This is followed by the genotype probability of each sample/individual. This can be done with 3 or 2 columns per sample/individual.
If the genotype probabilities are given in 3 columns, the sum must be 1 (rounding errors are corrected).
If genotype probabilities are given in 2 columns, the probability for the missing third column is substituted so that the sum equals 1. (see 1b option-c flag to determine whether third column is given or needs to be calculated)
Missing or inaccurate imputations are to be indicated by setting at least the first of the genotype probabilities to a negative value, e.g., [-1 0 0] (or [-1 0] if only 2 columns are given). If all three columns are provided, it is also possible to set all values to 0, i.e., [0 0 0].


The number of rows in the input-file equals to the number of genomic markers. 
The number of columns in the input-file equals to no. of leading columns + 2/3 time the no. of samples/individuals.


	--- Example input-file ---------------------------------------------

	rs00001 913 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0
	rs00002 402 0.01 0.99 0 0.01 0.99 0 0.01 0.99 0 0.01 0.99 0 0.01 0.99 0
	rs00003 644 0.333 0.334 0.333 0.333 0.334 0.333 0.333 0.334 0.333 0.333 0.334 0.333 0.333 0.334 0.333
	rs00004 222 0.25 0.5 0.25 0.25 0.5 0.25 0.25 0.5 0.25 0.25 0.5 0.25 0.25 0.5 0.25
	rs00005 221 0.47 0.18 0.35 0.89 0.02 0.09 0.03 0.96 0.01 0.94 0 0.06 0.62 0.34 0.04
	rs00006 955 0.975 0.002 0.023 0.52 0.154 0.326 0.309 0.21 0.481 0.48 0.509 0.011 0.969 0.004 0.027
	rs00007 518 0.63 0.14 0.23 0.86 0.09 0.05 0.35 0.24 0.41 0.01 0.23 0.76 0.76 0.05 0.19

number of rows: 7 (one for each marker)
number of columns: 3 leading columns + 15 genotype probability columns (3 genotype probability for each of 5 samples) = 18 columns 

----------------------------------------------------------------------
</details>
<details><summary>1b Parameter-file</summary>

>To set up the program, a text file called "params.txt" must exist in the same folder as the program.
>This "Parameter"-file defines the basic settings of the input files and the program control. It must have the following layout:



	-i [PATH LEADING TO DATA]
	-l [INTEGER]	default=5
	-c [BOOLEAN]	default=False
	-p [PATH]
	-m [PATH]
	-n [STRING]

Each parameter is followed by a single space and then the input value. The default values are provided above and will be inserted if the field is left empty. Paths are written in unix syntax. If you are on a different operating system, adapt them accordingly, e.g., on Windows, use foo\bar instead of foo/bar.
The parameters are explained in the following:

**-i**
Mandatory! Has the form [path relative to program]/[name of the file].impute2(.gz) with .gz optional if the file is compressed. Other compression formats are not supported at the moment.

**-l**
Number of leading columns before the actual input values. Defaults to 5.

**-c**
Flag used to determine, whether the third column for each sample needs to be calculated. Defaults to false. If the input file contains only two values per sample and the third needs to be calculated from the first two, set this to true. (False, True, 0, 1 are also valid inputs).

**-p**
Samples to exclude. Has the form [path relative to program]/[name of the file] and leads to a text file containing the IDs of samples to exclude (IDs for samples are determined via their order in the input file from left to right). 
All IDs must be written in one row and separated by comma. The ID of the first sample is 1. Negative values, zero, and values that exceed the number of samples in the input file are ignored.   

	--- Example "excluded_PROBE.txt" ----
	1,2

**-m**
Markers to exclude. Has the form [path relative to program]/[name of the file] and leads to a text file containing the IDs of markers to exclude. All IDs must be written in one row and separated by comma. 

	---  Example "excluded_SNP.txt"	------
	rs00001,rs00003,rs00004
	
**-n**
Names for the leading columns. Need to be separated by comma.

	--- Example			---------------------------
	SNP_no,SNP,position,minor_allele,major_allele.
 
If left empty or if the number of the specified column names does not correspond to the specified number of leading columns, default column names will be used. Per default, colX will be used for each column with X being the column number, starting with 0. 
Columns 2 and 3 will always be set to 'SNP' and 'position', regardless of whether default is used or a valid set of column names was given (see also comment on the first three leading columns in 1a).
Column names should not contain spaces to avoid problems reading the output file elsewhere.


> If no parameter file is provided, the program will ask for the parameters via the command line. They need to be provided in the same format as described above.

	--- Example "parameter"-file ----------------------------------------

	-i test1.imputed
	-l 3
	-c 0
	-p excluded_PROBE.txt
	-m excluded_SNP.txt
	-n SNP_no,SSSS,PPPPPP
	
</details>
</details>
<details><summary>2 Invoking the program</summary>

> Set the working directory to the folder containing the program and the "parameter"-file.
> To invoke the program use the following command structure:

	runfile('[NAME OF PROGRAM].py', args='-f params.txt')

> Alternatively, run the program without the parameter file or double-click the .exe if provided (in Windows). The input of the control parameters is queried during the program run (see 1b).
</details>

<details><summary>3 Output</summary>

> The result file **[NAME OF DATASET].accuracy** will be saved in the same folder as the original dataset. 
> Negative values (-9) are output if no accuracy index could be determined.
> Markers are ordered according to their position.  


The outfile contains the following information on any genomic marker (one each row) of the input file: 

	[leading columns as in the input-file] N MAF Iam_chance Iam_hwe hiQ accuracy info_IMPUTE r2_MACH r2_BEAGLE	

	N			is the number of samples used to determine the accuracy indices 
	MAF 			minor allele frequency used
	Iam_chance		1st accuracy index
	Iam_hwe		2nd accuracy index
	hiQ			3rd accuracy index
	accuracy		classification if genomic region (see below) 
	info_IMPUTE	4th accuracy index
	r2_MACH		5th accuracy index
	r2_BEAGLE		6th accuracy index

> 
 
	--- Example Output-file ---------------------------------------------

	created at 22.02.2022 20:02:22
	SNP_no SNP position N MAF Iam_chance Iam_hwe hiQ accuracy info_IMPUTE r2_MACH r2_BEAGLE
	5 rs00005 221 3 25.3% 0.656 0.576 0.84 tepid 0.481 0.336 -9
	2 rs00002 402 3 49.5% 0.97 0.968 0.929 tepid 0.98 0.0 -9
	7 rs00007 518 3 54.0% 0.297 0.249 0.688 tepid -0.066 0.585 -9
	6 rs00006 955 3 29.4% 0.4 0.3 0.814 tepid 0.084 0.503 -9

<details><summary>Classification of genomic regions</summary>
The exponentially weighted moving average (EWMA) is used to determine the average of the imputation accuracy (by Iam hiQ) of markers around the considered SNP. Based on this, genomic regions are categorized from "cold" to "very hot", the latter indicating massively inaccurate imputation.

	Iam = NA      or   hiQ = NA      --> accuracy = "NA"
	Iam > 0.47    and  hiQ > 0.97    --> accuracy = "cold"
	Iam < 0.47    or   hiQ < 0.97    --> accuracy = "tepid"
	Iam < 0.47    and  hiQ < 0.97    --> accuracy = "hot"
	Iam < 0.47/2  and  hiQ < 0.97/2  --> accuracy = "very hot"

</details>
</details>




